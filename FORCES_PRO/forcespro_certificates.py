# path to certificates file that contains client certificate to authenticate for codegen server (path separator on Windows is "\\"). To use default set to empty
codegen=""
# path to certificates file that contains client certificate to authenticate for storage (path separator on Windows is "\\"). To use default set to empty
storage=""
# path to certificates file that contains client certificate to authenticate for secondary storage (path separator on Windows is "\\"). To use default set to empty
secondary_storage=""
