%% Interface between lineat MPC and FORCESPRO QP Solver (internal)
%
%   The method calls FORCESPRO QP solver MEX file when "mpcmoveForces" is used
%   in simulation, not code generation.

%   Author(s): Rong Chen, MathWorks Inc.
%
%   Copyright 2019-2022 The MathWorks, Inc.
