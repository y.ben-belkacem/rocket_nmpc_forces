classdef forcesDenseQPBuildable < coder.ExternalDependency
%% FORCESPRO "mpc-toolbox-plugin" code generation utility

%   Author(s): Rong Chen, MathWorks Inc.
%
%   Copyright 2019-2022 The MathWorks, Inc.

    methods (Static)
        
        function name = getDescriptiveName(~)
            name = 'forcesDenseQPBuildable';
        end
        
        function b = isSupportedContext(context)
            b = context.isMatlabHostTarget();
        end        
        
        function updateBuildInfo(buildInfo, buildConfig)
            coder.allowpcode('plain');
            % solver name
            solvername = 'customForcesDenseQP';
            
            forcesUpdateBuildInfo(buildInfo, buildConfig, solvername, false);
        end
        
        function [x, status] = forcesDenseQpGeneral(H, f, A, b, x0)
            FORCES_SolverName = 'customForcesDenseQP';
            headerName = [FORCES_SolverName '.h'];
            coder.cinclude(headerName);
            coder.cinclude([FORCES_SolverName '_memory.h']);
            % define memory pointer
            memptr = coder.opaque([FORCES_SolverName '_mem *'],'HeaderFile',headerName);
            memptr = coder.ceval([FORCES_SolverName '_internal_mem'],uint32(0));
            % inputs based on C interface defined in the header file
            params = struct('H',H,'f',f,'A',-A,'b',-b);
            coder.cstructname(params,[FORCES_SolverName '_params'],'extern','HeaderFile',headerName);
            output = struct('DecisionVariables', zeros(length(f),1));
            coder.cstructname(output,[FORCES_SolverName '_output'],'extern','HeaderFile',headerName);
            SolverInfo = struct('it', int32(0), 'it2opt', int32(0), 'res_eq', 0, 'res_ineq', 0, 'pobj', 0, 'dobj', 0, 'dgap', 0, 'rdgap', 0, 'mu', 0, 'mu_aff', 0, 'sigma', 0, 'lsit_aff', int32(0), 'lsit_cc', int32(0), 'step_aff', 0, 'step_cc', 0, 'solvetime', 0, 'solver_id', int32(zeros(8, 1)));
            coder.cstructname(SolverInfo,[FORCES_SolverName '_info'],'extern','HeaderFile',headerName);
            FILE = coder.opaque('FILE *','NULL','HeaderFile',headerName);
            exitflag = int32(0); %#ok<NASGU>
            % generate code with solver DLL/LIB
            exitflag = coder.ceval([FORCES_SolverName '_solve'],coder.ref(params),coder.ref(output),coder.ref(SolverInfo),memptr,FILE);
            % get solution
            x = output.DecisionVariables;
            % Converts the "flag" output to "status" required by the MPC controller.
            switch double(exitflag)
                case 1
                    status = double(SolverInfo.it);
                case 0
                    status = 0;
                otherwise
                    status = -2;
            end
            % Always return a non-empty x of the correct size.  When the solver fails,
            % one convenient solution is to set x to the initial guess.
            if status <= 0
                x = x0;
            end
        end

        function [x, status] = forcesDenseQpFast(H, f, A, b, x0)
            FORCES_SolverName = 'customForcesDenseQP';
            headerName = [FORCES_SolverName '.h'];
            coder.cinclude(headerName);
            coder.cinclude([FORCES_SolverName '_memory.h']);
            % define memory pointer
            memptr = coder.opaque([FORCES_SolverName '_mem *'],'HeaderFile',headerName);
            memptr = coder.ceval([FORCES_SolverName '_internal_mem'],uint32(0));
            % inputs based on C interface defined in the header file
            params = struct('H',H,'f',f,'A',-A,'b',-b);
            coder.cstructname(params,[FORCES_SolverName '_params'],'extern','HeaderFile',headerName);
            output = struct('DecisionVariables', zeros(length(f),1));
            coder.cstructname(output,[FORCES_SolverName '_output'],'extern','HeaderFile',headerName);
            SolverInfo = struct('it', int32(0), 'it2opt', int32(0), 'res_primal', 0, 'res_dual', 0, 'pobj', 0, 'dobj', 0, 'dgap', 0, 'rdgap', 0, 'solvetime', 0, 'solver_id', int32(zeros(8, 1)));
            coder.cstructname(SolverInfo,[FORCES_SolverName '_info'],'extern','HeaderFile',headerName);
            FILE = coder.opaque('FILE *','NULL','HeaderFile',headerName);
            exitflag = int32(0); %#ok<NASGU>
            % generate code with solver DLL/LIB
            exitflag = coder.ceval([FORCES_SolverName '_solve'],coder.ref(params),coder.ref(output),coder.ref(SolverInfo),memptr,FILE);
            % get solution
            x = output.DecisionVariables;
            % Converts the "flag" output to "status" required by the MPC controller.
            switch double(exitflag)
                case 1
                    status = double(SolverInfo.it);
                case 0
                    status = 0;
                otherwise
                    status = -2;
            end
            % Always return a non-empty x of the correct size.  When the solver fails,
            % one convenient solution is to set x to the initial guess.
            if status <= 0
                x = x0;
            end
        end        
    end
end
