classdef forcesSparseQPBuildable < coder.ExternalDependency
%% Interface between linear MPC and FORCESPRO QP Solver (internal)

%   Author(s): Rong Chen, MathWorks Inc.
%
%   Copyright 2019-2022 The MathWorks, Inc.

    methods (Static)
        
        function name = getDescriptiveName(~)
            name = mfilename;
        end
        
        function b = isSupportedContext(context)
            b = context.isMatlabHostTarget();
        end        
        
        function updateBuildInfo(buildInfo, buildConfig)
            coder.allowpcode('plain');
            % solver name
            solvername = 'customForcesSparseQP';
            
            forcesUpdateBuildInfo(buildInfo, buildConfig, solvername, false);
        end
        
        function [output,status,SolverInfo] = forcesSparseQP(coredata,onlinedata,x,yref,uref,md,trueLastMV,warmstart)
            %#codegen
            % C interface is defined in the header file
            solvername = coredata.SolverName;
            headerName = [solvername '.h'];
            coder.cinclude(headerName);
            coder.cinclude([solvername '_memory.h']);
            % define memory pointer
            memptr = coder.opaque([solvername '_mem *'],'HeaderFile',headerName);
            memptr = coder.ceval([solvername '_internal_mem'],uint32(0));
            % define solver input information
            params = forcesSparseGetParamValues(coredata,onlinedata,x,yref,uref,md,trueLastMV,warmstart);
            coder.cstructname(params,[solvername '_params'],'extern','HeaderFile',headerName);
            output = struct('DecisionVariables', zeros(coredata.fLength(end),1));
            coder.cstructname(output,[solvername '_output'],'extern','HeaderFile',headerName);
            if ( coredata.CacheStatus == forcesMpcCacheStatus.Inactive )
                % Fast QP solver
                SolverInfo = struct('it', int32(0), 'it2opt', int32(0), 'res_primal', 0, 'res_dual', 0, 'pobj', 0, 'dobj', 0, 'dgap', 0, 'rdgap', 0, 'solvetime', 0, 'solver_id', int32(zeros(8, 1)));
            else                
                % General QP solver
                SolverInfo = struct('it', int32(0), 'it2opt', int32(0), 'res_eq', 0, 'res_ineq', 0, 'pobj', 0, 'dobj', 0, 'dgap', 0, 'rdgap', 0, 'mu', 0, 'mu_aff', 0, 'sigma', 0, 'lsit_aff', int32(0), 'lsit_cc', int32(0), 'step_aff', 0, 'step_cc', 0, 'solvetime', 0, 'solver_id', int32(zeros(8, 1)));
            end
            coder.cstructname(SolverInfo,[solvername '_info'],'extern','HeaderFile',headerName);
            FILE = coder.opaque('FILE *','NULL','HeaderFile',headerName);
            exitflag = int32(0);
            % generate code with solver DLL/LIB
            exitflag = coder.ceval([solvername '_solve'],coder.ref(params),coder.ref(output),coder.ref(SolverInfo),memptr,FILE);
            status = double(exitflag);
        end
        
    end
end
