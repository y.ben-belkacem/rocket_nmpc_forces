% Post process options for the FORCESPRO linear MPC toolbox plugin.
% Only for internal use.
%
% This file is part of the FORCESPRO client software for Matlab.
% (c) embotech AG, 2022, Zurich, Switzerland. All rights reserved.
