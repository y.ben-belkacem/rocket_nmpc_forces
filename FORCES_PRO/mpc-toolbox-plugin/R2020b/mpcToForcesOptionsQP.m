% mpcToForcesOptionsQP specifies options for "mpcToForces"
%
% Use this object to enable online MPC features and adjust FORCESPRO QP
% solver settings using either the dense or sparse QP formulation. 
%
% See also mpcToForcesOptions, mpcToForcesOptionsDenseQP, mpcToForcesOptionsSparseQP.
% Copyright 2022 The MathWorks, Inc. and Embotech AG
