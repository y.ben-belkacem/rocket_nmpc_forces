classdef forcesNLPBuildableMultistage < coder.ExternalDependency
    %% FORCESPRO "mpc-toolbox-plugin" code generation utility
    
    %   Author(s): Rong Chen, MathWorks Inc.
    %
    %   Copyright 2020-2022 The MathWorks, Inc.
    
    methods (Static)
        
        function name = getDescriptiveName(~)
            name = mfilename;
        end
        
        function b = isSupportedContext(context)
            b = context.isMatlabHostTarget();
        end
        
        function updateBuildInfo(buildInfo, cfg)
            % solver name
            classname = mfilename;
            idx = strfind(classname,'Buildable');
            solvername = classname(1:idx-1);
            
            forcesUpdateBuildInfo(buildInfo, cfg, solvername, true);
        end
        
        function [output,status,SolverInfo] = forcesNLP(coredata,onlinedata,x,lastMV)
            %#codegen
            % C interface is defined in the header file
            solvername = coredata.SolverName;
            headerName = [solvername '.h'];
            coder.cinclude(headerName);
            coder.cinclude([solvername '_memory.h']);
            coder.cinclude([solvername '_adtool2forces.h']);
            % define memory pointer
            memptr = coder.opaque([solvername '_mem *'],'HeaderFile',headerName);
            memptr = coder.ceval([solvername '_internal_mem'],uint32(0));
            % define solver input information (params, file and casadi)
            params = forcesnlmpcMultistageGetParamValues(coredata,onlinedata,x,lastMV);
            coder.cstructname(params,[solvername '_params'],'extern','HeaderFile',headerName);
            fp = coder.opaque('FILE *','NULL','HeaderFile',headerName);
            % need define extern int solvername_adtool2forces(haomin_float *x, haomin_float *y, haomin_float *l, haomin_float *p, haomin_float *f, haomin_float *nabla_f, haomin_float *c, haomin_float *nabla_c, haomin_float *h, haomin_float *nabla_h, haomin_float *hess, solver_int32_default stage, solver_int32_default iteration);
            casadi = coder.opaque([solvername '_extfunc'],['&' solvername '_adtool2forces'],'HeaderFile',headerName);
            % define solver output information (output, exitflag, info)
            output = struct('MVopt',zeros(coredata.p*coredata.nmv,1),'Xopt',zeros((coredata.p+1)*coredata.nx,1),'z',zeros(coredata.nz,1));
            coder.cstructname(output,[solvername '_output'],'extern','HeaderFile',headerName);
            if strcmp(coredata.SolverType,'SQP')
                SolverInfo = struct('it', int32(0), 'res_eq', 0, 'rsnorm', 0, 'pobj', 0, 'solvetime', 0, 'fevalstime', 0, 'QPtime', 0, 'QPit', int32(0), 'QPexitflag', int32(0), 'solver_id', int32(zeros(8, 1)));
            else
                SolverInfo = struct('it', int32(0), 'it2opt', int32(0), 'res_eq', 0, 'res_ineq', 0, 'rsnorm', 0, 'rcompnorm', 0, 'pobj', 0, 'mu', 0, 'solvetime', 0, 'fevalstime', 0, 'solver_id', int32(zeros(8, 1)));
            end
            coder.cstructname(SolverInfo,[solvername '_info'],'extern','HeaderFile',headerName);
            exitflag = int32(0);
            % generate code with solver DLL/LIB
            exitflag = coder.ceval([solvername '_solve'],coder.ref(params),coder.ref(output),coder.ref(SolverInfo),memptr,fp,casadi);
            status = double(exitflag);
        end
        
    end
    
end
