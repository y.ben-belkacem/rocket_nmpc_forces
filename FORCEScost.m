function J = FORCEScost(z)
    % z = [u; p; v; q; w] 
           % 123 u
           % 456 p
           % 789 v
           % 10 11 12 13 q
           % 14 15 16 w

    % minimize rotation about z => min (qx · qy + qz · qs)
    % minimize rotation about y => min (qy · qz − qx · qs)
    
    J = [(z(4)-5.0); % minimizing vertical distance
         z(11)*z(10)-z(12)*z(13);  % minimizing pitch and yaw
         z(11)*z(12)+z(13)*z(10)]; %dim = 3
    

end

