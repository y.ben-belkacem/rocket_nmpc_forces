function Q = Quaternion_Propagation(omega)
%Q Summary of this function goes here
%   Detailed explanation goes here
Q=zeros(4,4);

Q(1,1) = 0;
Q(1,2) = -omega(1);
Q(1,3) = -omega(2);
Q(1,4) = -omega(3);


Q(2,1) = omega(1);
Q(2,2) = 0 ;
Q(2,3) = omega(3);
Q(2,4) = -omega(2);



Q(3,1) = omega(2);
Q(3,2) = -omega(3);
Q(3,3) = 0;
Q(3,4) = omega(1);



Q(4,1) = omega(3);
Q(4,2) = omega(2);
Q(4,3) = -omega(1);
Q(4,4) = 0;


end

