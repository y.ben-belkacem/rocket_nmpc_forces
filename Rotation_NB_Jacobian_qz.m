function r = Rotation_NB_Jacobian_qz(q)
%R Summary of this function goes here
%   Detailed explanation goes here
module = sqrt( q(1)^2 + q(2)^2 + q(3)^2 + q(4)^2 );

q(1) = q(1)/module;
q(2) = q(2)/module;
q(3) = q(3)/module;
q(4) = q(4)/module;


r(1,1) = -2*(q(4)*2);
r(1,2) = -2*q(1);
r(1,3) = 2*(q(2));

r(2,1) = 2*(q(1));
r(2,2) = -2*(q(4)*2);
r(2,3) = 2*(q(3));

r(3,1) = 2*(q(2));
r(3,2) = 2*(q(3));
r(3,3) = 0;

end

