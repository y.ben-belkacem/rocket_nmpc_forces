function dx = dynamics(x,u)
% x = px py pz vx vy vz qw qx qy qz wx wy wz 
% u = Ux Uy Uz
% difference between state T and command U is T is a delayed command
%DYNAMICS Summary of this function goes here
%   Detailed explanation goes here
% QUATERNION IS DEFINED AS q = [qw qx qy qz]'
J1 = 0.0124;
J2 = 0.0644;
J3 = 0.0644;
m = 1.2;
g = [-9.81,0,0]';
rG = [0.3,0,0]';

dx = [ x(4);
       x(5);
       x(6);
      %dv
      ( (1 - 2*(x(9)*x(9) + x(10)*x(10))) * u(1) + (    2*( x(8)*x(9) - x(10)*x( 7))) * u(2) + (    2*(x(8)*x(10) + x(9)*x(7)))*u(3) )/m + g(1);  
      ( (    2*(x(8)*x(9) + x(10)*x( 7))) * u(1) + (1 - 2*( x(8)*x(8) + x(10)*x(10))) * u(2) + (    2*(x(9)*x(10) - x(8)*x(7)))*u(3) )/m + g(2);
      ( (    2*(x(8)*x(10)- x( 9)*x( 7))) * u(1) + (    2*( x(9)*x(10)+ x( 8)*x( 7))) * u(2) + (1 - 2*(x(8)*x( 8) + x(9)*x(9)))*u(3) )/m + g(3);
      %quat
      0.5*(-x(11)*x(8) - x(12)*x(9) - x(13)*x(10));
      0.5*( x(11)*x(7) + x(13)*x(9) - x(12)*x(10));
      0.5*( x(12)*x(7) - x(13)*x(8) + x(11)*x(10));
      0.5*( x(13)*x(7) + x(12)*x(8) - x(11)*x( 9)); 

      %w
      (1/J1)*(              x(13)*J2*x(12) - x(12)*J3*x(13));                %wx
      (1/J2)*( u(3)*rG(1) - x(13)*J1*x(11) + x(11)*J3*x(13));   %wy
      (1/J3)*(-u(2)*rG(1) + x(12)*J1*x(11) - x(11)*J2*x(12))   %wz
      ];
      
end

