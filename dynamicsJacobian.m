function [A, B] = dynamicsJacobian(x,~)

% QUATERNION IS DEFINED AS q = [qw qx qy qz]'

Iyy = 0.06547;
Izz = Iyy; 
Ixx = 0.03294;

m = 1.2;

I = [Ixx 0 0;
     0 Iyy 0;
     0 0 Izz];

g = [-9.81,0,0]';

rG = [0.5,0,0]';

tr = 0.05;


v = x(4:6);
q = x(7:10);
w = x(11:13);
T = x(14:16);



Jv = [zeros(3,3);
      eye(3);
      zeros(3,4)';
      zeros(3,3);
      zeros(3,3);
       ]';

Jdv =(1/m)*[zeros(3,3);
      zeros(3,3);
      (Rotation_NB_Jacobian_qw(q)*T)';
      (Rotation_NB_Jacobian_qx(q)*T)';
      (Rotation_NB_Jacobian_qy(q)*T)';
      (Rotation_NB_Jacobian_qz(q)*T)';
      zeros(3,3);
      Rotation_NB(q)';
      ]'; 

Jdq = [
       zeros(4,3)';
       zeros(4,3)';
       0.5*Quaternion_Propagation(w)';
       0.5*[-q(2), -q(3), -q(4);
             q(1), -q(4),  q(3);
             q(4),  q(1), -q(2);
            -q(3),  q(2),  q(1)]';
       zeros(4,3)'
        
        ]';

JwcrossJw = zeros(3,3);
JwcrossJw(1,1) = -w(3)*I(2,1) + w(2)*I(3,1);
JwcrossJw(1,2) = -w(3)*I(2,2) +I(3,:)*w + w(2)*I(3,2);
JwcrossJw(1,3) = -I(2,:)*w - w(3)*I(2,3) + w(2)*I(3,3);
JwcrossJw(2,1) = w(3)*I(1,1) - I(3,:)*w - w(1)*I(3,1);
JwcrossJw(2,2) = w(3)*I(1,2) - w(1)*I(3,2);
JwcrossJw(2,3) = I(1,:)*w + w(3)*I(1,3) - w(1)*I(3,3);
JwcrossJw(3,1) = -w(2)*I(1,1) + I(2,:)*w + w(1)*I(2,1);
JwcrossJw(3,2) = -I(1,:)*w - w(2)*I(1,2) + w(1)*I(2,2);
JwcrossJw(3,3) = -w(2)*I(1,3) + w(1)*I(2,3);

JTcrossrG = [ 0    ,   rG(3),  -rG(2);
             -rG(3),       0,   rG(1);
              rG(2),  -rG(1),      0];


Jdw = [zeros(3,3);
       zeros(3,3);
       zeros(3,4)';
       (-I\JwcrossJw)';
       (I\JTcrossrG)';
      ]';




A = [ %Jf(x,u)/x
    %px py pz vx vy vz qw qx qy qz wx wy wz Tx Ty Tz
    %----------------------------------------------; vx
                          Jv                       ; %vy 
    %----------------------------------------------; vz

    %----------------------------------------------; %dvx
                          Jdv                      ; %dvy
    %----------------------------------------------; dvz

    %----------------------------------------------; %dqw
                          Jdq                      ; %dqx
    %----------------------------------------------; %dqy
    %----------------------------------------------; %dqz

    %----------------------------------------------; %dwx
                          Jdw                      ; %dwy
    %----------------------------------------------; %dwz

     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,-1/tr, 0, 0; %dTx
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1/tr, 0; %dTy           JdT
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1/tr  %dTz
    %px py pz vx vy vz qw qx qy qz wx wy wz Tx Ty Tz
     ];


B = zeros(16,3);
B(14,1) = 1/tr;
B(15,2) = 1/tr;
B(16,3) = 1/tr;


end

