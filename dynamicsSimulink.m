function [out] = dynamicsSimulink(p)
% x = px py pz vx vy vz qw qx qy qz wx wy wz Tx Ty Tz
% u = Ux Uy Uz
% difference between state T and command U is T is a delayed command
%DYNAMICS Summary of this function goes here
%   Detailed explanation goes here
% QUATERNION IS DEFINED AS q = [qw qx qy qz]'
    x = p(1:13);
    u = p(14:16);
    
    out = dynamics(x,u);

end





