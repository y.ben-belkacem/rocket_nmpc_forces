function h = eval_const(z)

    h = [ 2*(  z(12)*z(13)   -  z(11)*z(10) ) ; %pitch
         -2*(  z(11)*z(12)   +  z(13)*z(10) ) / (1-z(11)*z(11)- z(13)*z(13))]; %yaw
end