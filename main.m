clc;
clear;


max_tilt = deg2rad(10);
m = 1.2;
g = 9.81;
Tmax = m*g;

model = {};

nx = 13;
nu = 3;


Tf = 3;
PredictionHorizon = 30;
ControlHorizon=PredictionHorizon - 1;

model.N = PredictionHorizon;
model.nvar = (nx + nu);
model.neq = 13;
model.nh = 2;   % number of nonlinear inequality constraints
model.npar = 0; % number of runtime parameters


Tsim = 8;
Ts = Tf/(model.N);
Ns = Tsim/(Tf/(model.N-1));   % simulation length?

integrator_stepsize = Ts;

% cost function
model.LSobjective = @FORCEScost;

% state space model
%model.continuous_dynamics = @dynamics;

continuous_dynamics = @(x,u) dynamics(x,u);
model.eq = @(z) RK4( z(4:16), z(1:3), continuous_dynamics, integrator_stepsize, [], 1 );
%model.continuous_dynamics = @dynamics;

model.E =  [zeros(nx,nu),eye(nx)]; % states selection from vector z

model.ineq = @eval_const;

model.hu = [sin(deg2rad(5)),     tan(deg2rad(5))];
model.hl = [-sin(deg2rad(5)),    -tan(deg2rad(5))]; %quaternions constrained to 0, T-mg=0


model.ub = [Tmax*2              ,  Tmax*sin(max_tilt)*ones(1,2),  +20,  +20,   +20, +2,  +3.0,  +3, +1,  +1, +1,  +1,  +3,  +3,  +3];
model.lb = [0,                  -Tmax*sin(max_tilt)*ones(1,2),  0.0,  -20,   -20, -2,  -3.0,   -3, -1,  -1, -1,  -1,  -3,  -3,  -3];

%model.objectiveN = @(z) 10*model.objective(z);

codeoptions = getOptions('RocketSolver');

codeoptions.nlp.integrator.type = 'ERK4';
codeoptions.nlp.integrator.Ts = Ts;
codeoptions.nlp.integrator.nodes = 5;
codeoptions.maxit = 100;                                    % Maximum number of iterations of inner QP solver
codeoptions.printlevel = 0;                                 % Use printlevel = 2 to print progress (but not for timings)
codeoptions.optlevel = 3; 
codeoptions.overwrite = 1;
% Options for IP solver
codeoptions.solvemethod = 'PDIP_NLP'; 
codeoptions.nlp.hessian_approximation = 'gauss-newton';     % Gauss-Newton hessian approximation of nonlinear least-squares objective 
codeoptions.sqp_nlp.maxqps = 1;     % Maximum number of quadratic problems to be solved in one solver call
codeoptions.MEXinterface.dynamics = 1; % Generate a MEX entrypoint for the nonlinear dynamics of the rocket
codeoptions.BuildSimulinkBlock = 1;

model.xinitidx = 4:16;   % indices affected by initial condition
%model.xfinalidx = [4 7 10:13];  % indices affected by final condition

x0=[15, 0, 0,        -0.1,0.1,0,          cos(deg2rad(5/2)),0,sin(deg2rad(5/2)),0,            0,0.1,0.1]';
% 
% p0 = [10,0,0]';
% v0 = [0,0,0]';
% q0 = [1,0,0,0]';
% w0 = [0,0,0]';
% T0 = [0,0,0]';
% 
% x0 = [p0;v0;q0;w0;T0];
u0 = [11.772000;0;0];

model.xinit = x0;
%model.xfinal = [0; 0; 1; 0; 0; 0];

codeoptions.nlp.TolStat = 1e-1; % Tolerance on gradient of Lagrangian
codeoptions.nlp.TolEq = 1e-1;   % Tolerance on equality constraints
codeoptions.nlp.TolIneq = 1e-1; % Tolerance on inequality constraints
codeoptions.nlp.TolComp = 1e-1; % Tolerance on complementarity

output1 = newOutput('u', 1, 1:3);
FORCES_NLP(model, codeoptions, output1);


% x0i=model.lb+(model.ub-model.lb)/2;
% x0=repmat(x0i',model.N,1);
% problem.x0=x0; 
% %problem.xfinal = model.xfinal;
% 
% X = zeros(nx,Ns); X(:,1) = model.xinit;
% U = u0.*ones(nu,Ns);
% ITER = zeros(1,Ns);
% SOLVETIME = zeros(1,Ns);
% FEVALSTIME = zeros(1,Ns);
% 
% X = zeros(nx,Ns); X(:,1) = model.xinit;
% U = zeros(nu,Ns);
% ITER = zeros(1,Ns);
% SOLVETIME = zeros(1,Ns);
% FEVALSTIME = zeros(1,Ns);
% 
% problem.xinit = X(:,1);
% [output,exitflag,info] = RocketSolver(problem);